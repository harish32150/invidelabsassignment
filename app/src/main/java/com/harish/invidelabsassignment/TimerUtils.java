package com.harish.invidelabsassignment;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import java.util.concurrent.TimeUnit;

/**
 * Created by Harish on 29/03/17.
 */

public class TimerUtils {
  public static Observable<Long> getTimerForSeconds(int seconds) {
    return io.reactivex.Observable.intervalRange(0, (seconds + 1), 0, 1, TimeUnit.SECONDS)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }
}
