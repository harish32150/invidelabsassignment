package com.harish.invidelabsassignment;

import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import com.harish.invidelabsassignment.databinding.ActivityMainBinding;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class MainActivity extends AppCompatActivity {

  private ActivityMainBinding binding;

  private int DURATION_SECS = 45 * 60; //45MINS

  private Typeface THIN_ROBOTO_TYPEFACE;

  private Disposable timerSubscription;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
    THIN_ROBOTO_TYPEFACE =
        Typeface.createFromAsset(getAssets(), getResources().getString(R.string.roboto_light));

    binding.circularView.setMaxProgress(DURATION_SECS);
    binding.timeRemainingText.setText(getTimeTextFromSeconds(DURATION_SECS));

    binding.startBtn.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        unSubscribeTimer();
        timerSubscription =
            TimerUtils.getTimerForSeconds(DURATION_SECS).subscribe(new Consumer<Long>() {
              @Override public void accept(@NonNull Long aLong) throws Exception {
                Log.d("harish", "time: " + aLong);
                binding.circularView.setProgress(aLong.intValue());
                binding.timeRemainingText.setText(
                    getTimeTextFromSeconds(DURATION_SECS - aLong.intValue()));
              }
            });
      }
    });

    binding.startBtn.setTypeface(THIN_ROBOTO_TYPEFACE);
    binding.timeRemainingText.setTypeface(THIN_ROBOTO_TYPEFACE);
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    unSubscribeTimer();
  }

  private String getTimeTextFromSeconds(int totalSecs) {
    int hours = totalSecs / 3600;
    int minutes = (totalSecs % 3600) / 60;
    int seconds = totalSecs % 60;
    return getString(R.string.time_remaining_format, hours, minutes, seconds);
  }

  private void unSubscribeTimer() {
    if (timerSubscription != null && !timerSubscription.isDisposed()) {
      timerSubscription.dispose();
    }
  }
}
