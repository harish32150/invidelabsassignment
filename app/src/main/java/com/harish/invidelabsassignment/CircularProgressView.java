package com.harish.invidelabsassignment;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import java.util.Locale;

/**
 * Created by Harish on 29/03/17.
 */
public class CircularProgressView extends View {

  private int maxProgress = 100;
  private int mSweepAngle = 0;
  private Paint mProgressPaint;
  private Paint mProgressBgPaint;
  private Paint mProgressFillPaint;
  private Paint mProgressTextPaint;

  private int width;
  private int height;

  public CircularProgressView(Context context) {
    super(context);
    init(context);
  }

  public CircularProgressView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }

  public CircularProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(context);
  }

  private void init(Context mContext) {
    mProgressPaint = new Paint();
    mProgressPaint.setColor(Color.parseColor("#689F38"));
    mProgressPaint.setAntiAlias(true);
    mProgressPaint.setStyle(Paint.Style.STROKE);
    mProgressPaint.setStrokeWidth(24);
    mProgressPaint.setStrokeCap(Paint.Cap.ROUND);

    mProgressBgPaint = new Paint();
    mProgressBgPaint.setColor(Color.parseColor("#DCEDC8"));
    mProgressBgPaint.setAntiAlias(true);
    mProgressBgPaint.setStyle(Paint.Style.STROKE);
    mProgressBgPaint.setStrokeWidth(24);

    mProgressFillPaint = new Paint();
    mProgressFillPaint.setColor(Color.parseColor("#99689F38"));
    mProgressFillPaint.setAntiAlias(true);

    mProgressTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    mProgressTextPaint.setColor(Color.parseColor("#4f4f4f"));
    mProgressTextPaint.setTextSize(144);
    mProgressTextPaint.setTextAlign(Paint.Align.CENTER);
    mProgressTextPaint.setTypeface(Typeface.createFromAsset(mContext.getAssets(),
        getResources().getString(R.string.roboto_thin)));
  }

  @Override protected void onDraw(Canvas canvas) {
    int centreX = getMeasuredWidth() / 2;
    int centreY = getMeasuredHeight() / 2;

    int radius = Math.min(getMeasuredWidth(), getMeasuredHeight());

    int paddingHorizontal = 0;
    int paddingVertical = 0;

    if (width > radius) {
      paddingHorizontal = width - radius;
      paddingHorizontal /= 2;
    } else {
      paddingVertical = height - radius;
      paddingVertical /= 2;
    }
    RectF rectF =
        new RectF(24 + paddingHorizontal, 24 + paddingVertical, width - paddingHorizontal - 24,
            height - paddingVertical - 24);

    canvas.drawArc(rectF, 360, 360, false, mProgressBgPaint);
    canvas.drawArc(rectF, 0, mSweepAngle, false, mProgressPaint);

    mProgressFillPaint.setShader(
        new RadialGradient(width / 2, height / 2, height / 3, Color.parseColor("#DCEDC8"),
            Color.parseColor("#689F38"), Shader.TileMode.REPEAT));

    canvas.drawArc(rectF, 0, mSweepAngle, true, mProgressFillPaint);

    String progress = String.format(Locale.getDefault(), "%s%c",
        String.valueOf((int) ((mSweepAngle * 100) / 360.0f)), '%');

    canvas.drawText(progress, centreX,
        centreY - ((mProgressTextPaint.descent() + mProgressTextPaint.ascent()) / 2),
        mProgressTextPaint);
  }

  @Override protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    width = MeasureSpec.getSize(widthMeasureSpec);
    height = MeasureSpec.getSize(heightMeasureSpec);
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
  }

  public void setMaxProgress(int maxProgress) {
    this.maxProgress = maxProgress;
  }

  public void setProgress(int progress) {
    mSweepAngle = (int) (360 * (float) progress / maxProgress);
    invalidate();
  }
}

